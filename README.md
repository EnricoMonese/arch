![Arch](./cover.png)

# Arch Installation Script

A script that installs Arch Linux so I don't have to do it.

## Instructions (kinda)

1. connect to internet using NetworkManager: `nmcli device wifi connect SSID_or_BSSID password password` or `nmtui`
2. download install script and config file
3. run script
4. reboot

## References

In no particular order

- [Installation guide](https://wiki.archlinux.org/index.php/Installation_guide)
- [github.com/MatMoul/archfi](https://github.com/MatMoul/archfi/blob/master/archfi)
- [github.com/picodotdev/alis](https://github.com/picodotdev/alis/blob/master/alis.sh)
- [Arch Linux Installation Guide 2020](https://www.youtube.com/watch?v=PQgyW10xD8s)
- [Arch Linux Installation (2020) | Full Guide](https://www.youtube.com/watch?v=QMBE5Kxb8Bg)
- [gitlab.com/eflinux/arch-basic/](https://gitlab.com/eflinux/arch-basic/-/blob/master/install-uefi.sh)
- [github.com/classy-giraffe/easy-arch](https://github.com/classy-giraffe/easy-arch)
- [github.com/lotw69/arch-scripts](https://github.com/lotw69/arch-scripts)
- [github.com/classy-giraffe/easy-arch](https://github.com/classy-giraffe/easy-arch/)
- [github.com/rickellis/Arch-Linux-Install-Guide](https://github.com/rickellis/Arch-Linux-Install-Guide)
- [github.com/johnynfulleffect/ArchMatic](https://github.com/johnynfulleffect/ArchMatic)
- [github.com/santigo-zero/csjarchlinux](https://github.com/santigo-zero/csjarchlinux)
- [github.com/SayanChakroborty/arch-installer](https://github.com/SayanChakroborty/arch-installer)
- [github.com/deepbsd/Farchi](https://github.com/deepbsd/Farchi)
- [github.com/mietinen/archer](https://github.com/mietinen/archer)
- [gitlab.com/C0rn3j/arch](https://gitlab.com/C0rn3j/arch/blob/master/install.sh)
